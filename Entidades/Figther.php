<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Figther
 *
 * @author pabhoz
 */
class Figther implements FigtherI{
    
    private $nombre;
    private $healtPoints;
    private $strenght;
    private $speed;
    private $defense;

    function __construct($nombre, $healtPoints, $strenght, $speed, $defense) {
        $this->nombre = $nombre;
        $this->healtPoints = $healtPoints;
        $this->strenght = $strenght;
        $this->speed = $speed;
        $this->defense = $defense;
    }
    
    function getNombre() : string{
        return $this->nombre;
    }

    function getHealtPoints() : float{
        return $this->healtPoints;
    }

    function getStrenght() : float{
        return $this->strenght;
    }

    function getSpeed() : float{
        return $this->speed;
    }

    function getDefense() : float{
        return $this->defense;
    }

    function setNombre($nombre){
        $this->nombre = $nombre;
    }

    function setHealtPoints($healtPoints) {
        $this->healtPoints = $healtPoints;
        print $this->getNombre()." ahora tiene ".$healtPoints." HP <br>";
    }

    function setStrenght($strenght) {
        $this->strenght = $strenght;
    }

    function setSpeed($speed){
        $this->speed = $speed;
    }

    function setDefense($defense) {
        $this->defense = $defense;
    }

    public function specialAttack(\FigtherI $f) {
        
    }

    public function attack(\FigtherI $f) {
        $str = $this->getStrenght();
        print $this->getNombre()." ataca a ".$f->getNombre()."<br>"; 
        $f->getHurt($str);
    }

    public function getHurt(float $dmg) {
        $tdmg = $dmg - ($this->getDefense() * 0.1);
        print $this->getNombre()." sufre ".$tdmg." de daño <br>";
        $this->setHealtPoints($this->getHealtPoints() - $tdmg);
    }

}
